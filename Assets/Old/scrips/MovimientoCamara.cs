﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCamara : MonoBehaviour {

	 float movimiento = 0.013f;
	
	float tiempo = 0f;

	// Update is called once per frame
	void Update () {
		tiempo = Time.time;
		

		if (tiempo < 22){
			transform.SetPositionAndRotation(new Vector3(movimiento + transform.position.x,transform.position.y,transform.position.z), transform.rotation);

		}
		
		if (tiempo > 14 && tiempo < 22){
			transform.SetPositionAndRotation(new Vector3(transform.position.x,transform.position.y - movimiento,transform.position.z), transform.rotation);
			transform.Rotate(Vector3.up * Time.deltaTime * 7f, Space.World);
		}

		if (tiempo > 20 && tiempo < 28){
			transform.SetPositionAndRotation(new Vector3(transform.position.x,transform.position.y + movimiento,transform.position.z), transform.rotation);
		}


	}
}
