﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public float velocidadX;
	public float velocidadY;
	public float maximo;
	public float posx;
	public float posy;

	// Update is called once per frame
	void Update () {
		
		posx = transform.position.x;
		posy = transform.position.y;

		posx = posx +velocidadX*Time.deltaTime;
		posy = posy +velocidadY*Time.deltaTime;

		transform.SetPositionAndRotation(new Vector3(posx,posy,transform.position.z), transform.rotation);

	}

	private void OnTriggerEnter2D(Collider2D other) {
		

		if(other.gameObject.tag == "Player"){
			velocidadX*=-1.5f;

			if(velocidadX>maximo){
				velocidadX = maximo;
			}else if(velocidadX<-maximo){
				velocidadX = -maximo;
			}

			velocidadY*=1.25f;

			if(velocidadY>maximo){
				velocidadY = maximo;
			}else if(velocidadY<-maximo){
				velocidadY = -maximo;
			
			}

		}

		if(other.gameObject.tag == "Pared"){
			velocidadY*=-1;

		}

		if(other.gameObject.tag == "ParedPlayer"){
			Debug.Log("Gol Enemy");
			
			
		}

		if(other.gameObject.tag == "ParedEnemy"){
			Debug.Log("Gol Player");
			
			
		}

	}

}
