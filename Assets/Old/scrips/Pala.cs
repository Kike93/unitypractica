﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pala : MonoBehaviour {

	public float velocidadY;

	public float limitYTop;
	public float limitYDown;

	public Ball ball;
	public float posy;
	
	// Update is called once per frame
	void Update () {
		posy = transform.position.y;
		
		posy += velocidadY*Time.deltaTime;
		
        if(posy> limitYTop){
            posy = limitYTop;
        }else if(posy < limitYDown){
           posy = limitYDown;
        }	

		posy = posy +velocidadY*Time.deltaTime;
 

		transform.SetPositionAndRotation(new Vector3(transform.position.x,posy,transform.position.z), transform.rotation);
	}
}
