﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Porteria : MonoBehaviour {

	public GameObject Ball;
	
	Vector3 position = new Vector3(0,0,0);
	// Update is called once per frame


	private void OnTriggerEnter2D(Collider2D other) {	

		if(other.gameObject.tag == "Ball"){
			Destroy(other.gameObject);
			Instantiate(Ball, position, Quaternion.identity);
			
		}
	}
}
