﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
 
public static class SaveLoad {
 
    public static List<Door> savedDoor = new List<Door>(); // Cambiar por diccionary
    public static List<Interruptor> savedInter = new List<Interruptor>();
         
    //it's static so we can call it from anywhere
    public static void SaveDoor(List<Door> d) {
        //SaveLoad.savedGames.Add(Door.current);
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create (Application.persistentDataPath + "/savedDoor.txt"); //you can call it anything you want
        bf.Serialize(file, d);
        file.Close();
    }   
     
    public static bool LoadDoor() {
        if(File.Exists(Application.persistentDataPath + "/savedDoor.txt")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedDoor.txt", FileMode.Open);
            SaveLoad.savedDoor = (List<Door>)bf.Deserialize(file);
            file.Close();
            return true;
        } else {
            return false;
        }
    }

    public static void SaveInter(List<Interruptor> k) {
        //SaveLoad.savedGames.Add(Door.current);
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create (Application.persistentDataPath + "/SavedInter.txt"); //you can call it anything you want
        bf.Serialize(file, k);
        file.Close();
    }   
     
    public static bool LoadInter() {
        if(File.Exists(Application.persistentDataPath + "/SavedInter.txt")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/SavedInter.txt", FileMode.Open);
            SaveLoad.savedInter = (List<Interruptor>)bf.Deserialize(file);
            file.Close();
            return true;
        } else {
            return false;
        }
    }
}