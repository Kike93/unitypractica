﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorClose : MonoBehaviour
{

    public GameObject changeDoor;

    private GameObject player;

    private Door doorOpen;

    void Start()
    {
        Debug.Log(Application.persistentDataPath);
        player = GameObject.FindGameObjectWithTag("Player");

        foreach (Door item in DiccionarioGlobal.d)
        {
            
            if(this.gameObject.name == item.getName()){
                
                doorOpen = item;
            }
        }

        if(doorOpen.getState()){
            changeDoor.SetActive(true);
            Destroy(this.gameObject);
        }

    }

    void Update(){
        if(doorOpen.getState()){
            changeDoor.SetActive(true);
            Destroy(this.gameObject);
        }

    }
    
}
