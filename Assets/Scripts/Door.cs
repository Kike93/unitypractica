﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Door
{
     public static Door current;
    private string name;
    private bool state;

    
    
    public Door(string newName, bool newState){
        name = newName;
        state = newState;
    }

    public string getName(){
        return name;
    }

    public bool getState(){
        return state;
    }   

    public void setState(bool changeS){
        state = changeS;
    } 
   
}
