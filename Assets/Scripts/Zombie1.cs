﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie1 : MonoBehaviour
{

    float speed = 1.5f;
    Vector3 movimiento;
    GameObject player;

    Animator anim;
    bool dead = false;
    bool walk = false;
    bool attack = false;
    bool idle = false;
    bool hurt = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if(!dead){
            if(detectPlayer()){
                attack = false;
                walk = true;
                if(!hurt){
                    followPlayer();
                }
                
            } else {
                walk = false;
                idle = true;

            }

            anim.SetBool("hurt", hurt);
            anim.SetBool("walk", walk);
            anim.SetBool("idle", idle);
            anim.SetBool("attack", attack);
           
        } else {
            anim.SetBool("dead", dead);
        }
    }

    void followPlayer(){
        this.transform.position += movimiento * Time.deltaTime;
    }


    bool detectPlayer(){

        float distance = this.transform.position.x - player.transform.position.x; 

        float altura = player.transform.position.y - this.transform.position.y; 

        if(distance <= 100 && distance >= -100 && altura <= 12 && altura >= -12){

            if(distance <= 12 && distance >= -12){
                attack = true;
            } else {
                attack = false;
                if(distance < 0){
                    transform.localRotation = Quaternion.Euler(0, 0, 0);
                    speed = 8f;
                    movimiento = new Vector3(speed, 0, 0);
                } else {
                    transform.localRotation = Quaternion.Euler(0, 180, 0);
                    speed = -8f;
                    movimiento = new Vector3(speed, 0, 0);
                }
                
                return true;
            }

            
        }
        return false;
    }

    bool arroundPlayer(){

        float distance = this.transform.position.x - player.transform.position.x;

        if(distance <= 100 && distance >= -100){
            if(distance < 0){
                transform.localRotation = Quaternion.Euler(0, 0, 0);
                speed = 8f;
                movimiento = new Vector3(speed, 0, 0);
            } else {
                transform.localRotation = Quaternion.Euler(0, 180, 0);
                speed = -8f;
                movimiento = new Vector3(speed, 0, 0);
            }
            
            return true;
        }

        return true;
    }

    
    void OnCollisionEnter2D(Collision2D other)
    {
       
        if(other.gameObject.tag == "Bullet"){
            Destroy(other.gameObject);
            hurt = true;
            idle = false;
            walk = false;
            Invoke("noHurt", 0.5f);
        }
    }

    void noHurt(){
        hurt = false;
         Debug.Log("dfsd");
    }
}
