﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiccionarioGlobal : MonoBehaviour
{

    public static List<Door> d = new List<Door>();
    public static List<Interruptor> k = new List<Interruptor>();
    // Start is called before the first frame update

    private Door door;
    void Awake()
    {

        if(SaveLoad.LoadDoor() && SaveLoad.LoadInter()){

            d = SaveLoad.savedDoor;
            k = SaveLoad.savedInter;

        } else {

            for (int i = 1; i < 26; i++)
            {
                if(i == 1 && i == 2 && i == 3 && i == 8){
                   k.Add( new Interruptor("IntClose"+i, false, i));

               } else{
                   k.Add( new Interruptor("IntClose"+i, false, 0));
               }
               
                
            }

             

            for (int i = 1; i < 26; i++)
            {

                door = new Door("Door"+i, false);
                d.Add( door );
            }

            saveD();
            saveK();
        }

    }


    public static void saveD(){

        SaveLoad.SaveDoor(d);
    }

    public static void saveK(){

        SaveLoad.SaveInter(k);
    }
}
