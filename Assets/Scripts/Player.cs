﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private float moveH;
    private float speed = 30;
    private float forceJump = 30;

    private float tiempoRetardo;


    private float dashTime;
    private int dashSpeed = 3;
    private float startDashTime = 0.8f;
    
    private bool OnGround;
     private bool canDash = true;
    public LayerMask suelo;
    public Transform pie;
    private float radio = 1f;
    private int saltos;
    private bool isDashing = false;
    Animator anim;
     private Rigidbody2D rb2d;

    public GameObject shotPrefab;

    public GameObject shotSpawn;

    private float rateShot = 0.2f;
    private float timeWaitShot = 0;
    
    // Start is called before the first frame update
    void Start()
    {   
        OnGround = true;
        saltos = 0;
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D> ();
        dashTime = startDashTime;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        move();


        anim.SetBool("Dash", isDashing);
        anim.SetFloat("Walk", Mathf.Abs (moveH));
        anim.SetBool("jump" ,!OnGround);

    }


    private void move(){
        if(Input.GetAxis("Dash") > 0.1 && !isDashing && canDash && OnGround){
            isDashing = true;
            
        }

        if(isDashing && OnGround && canDash){
            dash();
            
        } else {
            moveCharacter();
           
         }

    }

    private void moveCharacter(){
         moveH = Input.GetAxis("Horizontal");
            OnGround = Physics2D.OverlapCircle(pie.position, radio, suelo);

            if(moveH < 0){
                transform.localRotation = Quaternion.Euler(0, 180, 0);
            
            }else if(moveH > 0){
                transform.localRotation = Quaternion.Euler(0, 0, 0);
            }

            rb2d.velocity = new Vector2(moveH * speed, rb2d.velocity.y);

            if(OnGround){
                anim.SetBool("doubleJump" , false);
                saltos = 0;

                timeWaitShot += Time.deltaTime;

                if(moveH == 0 && Input.GetAxis("Fire1") > 0.1 && timeWaitShot > rateShot){
                    anim.SetBool("shot" , true);
                    anim.SetBool("shotMoving" , false);
                    shotting();
                    
                } else if (Input.GetAxis("Fire1") > 0.1 && timeWaitShot > rateShot){
                    anim.SetBool("shot" , false);
                    anim.SetBool("shotMoving" , true);
                    shotting();
                    
                } else{
                    anim.SetBool("shotMoving" , false);
                    anim.SetBool("shot" , false);
                }

            } else{

                timeWaitShot += Time.deltaTime;
                if(Input.GetAxis("Fire1") > 0.1 && timeWaitShot > rateShot){
                    anim.SetBool("jumpShot" , true);
                    shotting();
                    
                } else {
                    anim.SetBool("jumpShot" , false);
                }

                anim.SetBool("shotMoving" , false);
                anim.SetBool("shot" , false);
            }

            jumpFunction();
            
    }

    private void jumpFunction(){
        if(OnGround && Input.GetAxis("Jump") > 0.1){
                saltos++;
                rb2d.velocity += new Vector2(0,forceJump * 3);
                tiempoRetardo = Time.time;
            }
            
            if(OnGround == false && saltos < 2 && saltos > 0 && Input.GetAxis("Jump") > 0.1 && tiempoRetardo + 0.5 < Time.time){
                //tiempoRetardo = Time.time; Para mas saltos * Incluir mas animaciones *
                saltos++;
                rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
                rb2d.velocity += new Vector2(0, forceJump * 2);
                anim.SetBool("doubleJump" , true);
            }

    }

    private void shotting(){
        timeWaitShot = 0;
        Instantiate(shotPrefab, shotSpawn.transform.position, shotSpawn.transform.rotation);

    }


    private void dash(){
        if(dashTime <= 0){
                isDashing = false;
                canDash = false;
                Invoke("restartDash", 0.5f);
                
            } else {
                dashTime -= Time.deltaTime;
                
                if(moveH == 0) {
                    moveH = 1;
                    transform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                rb2d.velocity = new Vector2(moveH * speed * dashSpeed, rb2d.velocity.y);
            }
    }

     private void restartDash(){
        canDash = true;
        dashTime = startDashTime;
    }
}
