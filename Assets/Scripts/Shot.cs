﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{   

    private float speedShot = 85;
    private Rigidbody2D shotRB;
    private GameObject player;

    // Start is called before the first frame update
    void Awake()
    {
        shotRB = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        
    }

    // Update is called once per frame
    void Start()
    
    {   
        Debug.Log(player.transform.localScale.x);
        if(player.transform.rotation.y == 0){
            shotRB.velocity = new Vector2(speedShot, shotRB.velocity.y);

        } else {
            shotRB.velocity = new Vector2(-speedShot, shotRB.velocity.y);
        }
        
       
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "colisionPared"){
            Destroy(this.gameObject);

        }
    }
}
