﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Interruptor
{
    // Start is called before the first frame update
    private string name;
    private bool state;
    private int interruptor;
    public Interruptor(string newName, bool newState, int newInterruptor){
        name = newName;
        state = newState;
        interruptor = newInterruptor;
    }

    public string getName(){
        return name;
    }
    public int getInterruptor(){
        return interruptor;
    }   
    public bool getState(){
        return state;
    }   

    public void setState(bool changeS){
        state = changeS;
    } 

}
