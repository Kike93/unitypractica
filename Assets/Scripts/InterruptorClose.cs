﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterruptorClose : MonoBehaviour
{

    public GameObject interruptorOpen;
    private Door door;
    private Interruptor interruptor;
    private int x;
    private int cont = 0;
    void Start()
    {
        
        foreach (Interruptor item in DiccionarioGlobal.k)
        {   
            
            cont++;
            if(this.gameObject.name == item.getName()){
                x = cont;
                interruptor = item;
                door = DiccionarioGlobal.d[interruptor.getInterruptor()];
            }
        }
        Debug.Log(x);
        if(interruptor.getState()){
            interruptorOpen.SetActive(true);
            Destroy(this.gameObject);
        }

    }

    void Update(){
        if(interruptor.getState()){
            interruptorOpen.SetActive(true);
            Destroy(this.gameObject);
        }

    }
    private void OnTriggerStay2D(Collider2D other) {
        if(Input.GetAxis("Fire2") > 0.1 && other.tag == "Player"){
            //doorOpen.SetActive(true);
            interruptorOpen.SetActive(true);
            interruptor.setState(true);
            door.setState(true);
            DiccionarioGlobal.d[interruptor.getInterruptor()] = door;
            DiccionarioGlobal.k[x] = interruptor;
            DiccionarioGlobal.saveD();
            DiccionarioGlobal.saveK();
            Destroy(this.gameObject);
        }
    }
}
