﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorOpen : MonoBehaviour
{

    public int nextScene;

    private Door doorOpen;
    // Start is called before the first frame update
    void Start()
    {

        foreach (Door item in DiccionarioGlobal.d)
        {
            
            if(this.gameObject.name == item.getName()){
                
                doorOpen = item;
            }
        }

    }
    
    
    private void OnTriggerStay2D(Collider2D other) {

        if(Input.GetAxis("Fire2") > 0.1 && other.tag == "Player"){
            
            changeScene();
        }
    }

    private void changeScene(){

        SceneManager.LoadScene("Wold"+nextScene);

    }
}
